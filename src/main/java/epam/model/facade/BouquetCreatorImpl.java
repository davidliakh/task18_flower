package epam.model.facade;

import epam.model.bouquet.Bouquet;
import epam.model.bouquet.Flower;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.List;
import java.util.Map;

public class BouquetCreatorImpl implements BouquetCreator {
    private static Logger logger = LogManager.getLogger(BouquetCreatorImpl.class);
    private Bouquet bouquet;

    public BouquetCreatorImpl(Bouquet bouquet) {
        this.bouquet = bouquet;
    }

    public void prepareFlowers() {
        Map<Flower, Integer> flowers  = bouquet.getFlowers();
        logger.info("Готуємо квіти: ");
        for (Flower flower: flowers.keySet()) {
            logger.info(flower.getName() + " - " + flowers.get(flower)+" шт");
        }
    }

    public void prepareDecorations() {
        List<String> decorations = bouquet.getDecorations();
        logger.info("Готуємо декорації: ");
        for (String decoration: decorations) {
            logger.info(decoration);
        }
    }

    public void create() {
        logger.info("Створюємо букет " + bouquet.getBouquetType());
    }
}
