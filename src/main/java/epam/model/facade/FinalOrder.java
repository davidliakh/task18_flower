package epam.model.facade;

import epam.model.bouquet.Bouquet;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.List;

public class FinalOrder implements OrderFacade{
    private static Logger logger = LogManager.getLogger(Order.class);
    private Order order;
    private Box box;
    private Delivery delivery;

    public FinalOrder(Order order, Box box, Delivery delivery) {
        this.order = order;
        this.box = box;
        this.delivery = delivery;
    }

    @Override
    public void createOrder() {
        List<Bouquet> bouquets = order.getBouquets();
        for (Bouquet bouquet : bouquets) {
            BouquetCreator bouquetCreator = new BouquetCreatorImpl(bouquet);
            bouquetCreator.prepareFlowers();
            bouquetCreator.prepareDecorations();
            bouquetCreator.create();
        }
        if (box!=null) box.boxOrder(order);
        if (delivery!=null) delivery.deliverOrder(order);
        bouquets.forEach(logger::info);
    }
}
