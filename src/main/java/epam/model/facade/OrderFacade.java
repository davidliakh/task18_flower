package epam.model.facade;

import epam.model.bouquet.Bouquet;;

import java.util.List;
import java.util.Scanner;

public interface OrderFacade {
    void createOrder();
}
