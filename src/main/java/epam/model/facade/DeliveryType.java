package epam.model.facade;

import java.util.ResourceBundle;

public enum DeliveryType {
    NOVA_POSHTA("Nova_poshta"), MIST_EXPRESS("Mist_express");

    private String name;
    private double price;

    DeliveryType(String name) {
        ResourceBundle resourceBundle = ResourceBundle.getBundle("prices");
        this.name = name;
        this.price = Double.parseDouble(resourceBundle.getString(name));
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }
}
