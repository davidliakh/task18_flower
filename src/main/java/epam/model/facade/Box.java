package epam.model.facade;

import epam.model.util.PropertiesRead;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Box {
    private static Logger logger = LogManager.getLogger(Box.class);
    private PropertiesRead propertiesRead;

    public Box() {
        this.propertiesRead = new PropertiesRead("prices.properties");
    }

    public void boxOrder(Order order){
        order.addToPrice(Double.parseDouble(
                propertiesRead.getValue("boxing")));
        logger.info("Упаковуємо");
    }
}
