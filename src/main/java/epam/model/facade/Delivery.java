package epam.model.facade;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class Delivery {
    private static Logger logger = LogManager.getLogger(Delivery.class);
    private DeliveryType deliveryType;

    public Delivery(DeliveryType deliveryType) {
        this.deliveryType = deliveryType;
    }

    public void deliverOrder(Order order){
        order.addToPrice(deliveryType.getPrice());
        logger.info("Передаємо кур'єру\n" + "До сплати : " + order.getFinalPrice());
    }
}
