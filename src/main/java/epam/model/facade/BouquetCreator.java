package epam.model.facade;

public interface BouquetCreator {

    void prepareFlowers();

    void prepareDecorations();

    void create();
}
