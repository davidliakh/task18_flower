package epam.model.facade;

import epam.model.bouquet.Bouquet;
import epam.model.decor.BouquetDecorator;
import epam.model.decor.decorImplementation.DiscountDecorator;
import epam.model.observer.Card;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class Order {
    private static Logger logger = LogManager.getLogger(Order.class);
    private List<Bouquet> bouquets;
    private double price;

    public Order() {
        bouquets = new ArrayList<>();
    }

    public void addBouquetToOrder(Bouquet bouquet, Card card){
        DiscountDecorator discountDecorator = new DiscountDecorator(card);
        discountDecorator.setBouquet(bouquet);
        BouquetDecorator finalBouquet = new BouquetDecorator();
        finalBouquet.setBouquet(discountDecorator);
        bouquets.add(finalBouquet.getBouquet());
    }

    public void removeBouquetFromOrder(Bouquet bouquet){
        bouquets.remove(bouquet);
    }

    public List<Bouquet> getBouquets() {
        return bouquets;
    }

    public double getPrice() {
        return price = calculatePrice();
    }

    public double getFinalPrice() {
        return price;
    }

    private double calculatePrice(){
        double orderPrice = 0;
        for (Bouquet bouquet : bouquets) {
            orderPrice += bouquet.getPrice();
        }
        return  orderPrice;
    }

    public void addToPrice(Double price) {
        this.price += price;
    }

    @Override
    public String toString() {
        return "Order{" +
                "bouquets=" + bouquets +
                ", orderPrice=" + price +
                '}';
    }
}
