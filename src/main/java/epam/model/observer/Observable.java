package epam.model.observer;

import epam.model.facade.Order;
import epam.model.shop.City;

public interface Observable {

    void addObserver(Observer observer);

    void removeObserver(Observer observer);

    void notifyObserver(Order order);

    void notifyObserver(City city);

}
