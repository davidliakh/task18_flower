package epam.model.observer;

import epam.model.facade.Order;
import epam.model.shop.City;

public interface Observer {
    void handleEvent(Order order);
    void handleEvent(City city);
    public String getName();
    public Card getCard();
}
