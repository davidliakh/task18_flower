package epam.model.observer;

import epam.model.facade.Order;
import epam.model.shop.City;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Customer implements Observer{
    private static Logger logger = LogManager.getLogger(Customer.class);
    private String name;
    private Card card;

    public Customer(String name, Card card) {
        this.name = name;
        this.card = card;
    }

    @Override
    public void handleEvent(Order order) {
        logger.info("Dear customer " + name + ", your order was changed to "
                + order);
    }

    @Override
    public void handleEvent(City city) {
        logger.info("Dear customer " + name + ", city was set to "
                + city.getName());
    }

    public String getName() {
        return name;
    }

    public Card getCard() {
        return card;
    }
}
