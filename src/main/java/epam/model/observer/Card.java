package epam.model.observer;

public enum Card {
    GOLD("Gold", 0.6), SILVER("Silver", 0.4), PLATINUM("Platinum", 0.2),
    SIMPLE("Simple", 0.05) , BASIC("Basic", 0);

    private String name;
    private double discount;

    Card(String name, double discount) {
        this.name = name;
        this.discount = discount;
    }

    public String getName() {
        return name;
    }

    public double getDiscount() {
        return discount;
    }

    @Override
    public String toString() {
        return  name +"  -"+(int)((discount)*100)+"%";
    }
}
