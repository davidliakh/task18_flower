package epam.model.shop;

import epam.model.bouquet.Bouquet;
import epam.model.bouquet.BouquetType;
import epam.model.bouquet.bouquetImpl.wedding.ArtificialBouquet;
import epam.model.bouquet.bouquetImpl.wedding.ClassicRoseBouquet;
import epam.model.bouquet.bouquetImpl.wedding.DublerBouquet;

public class KyivShop  extends ShopFactory{

    @Override
    protected Bouquet createBouquet(BouquetType bouquetType) {
        switch (bouquetType){
            case DUBLER: return new DublerBouquet();
            case ARTIFICIAL: return new ArtificialBouquet();
            case CLASSIC_ROSE: return new ClassicRoseBouquet();
        }
        return null;
    }
}
