package epam.model.shop;

import epam.model.bouquet.Bouquet;
import epam.model.bouquet.BouquetType;
import epam.model.bouquet.bouquetImpl.birthday.BirthdayBouquet;
import epam.model.bouquet.bouquetImpl.wedding.ClassicRoseBouquet;
import epam.model.bouquet.bouquetImpl.wedding.DublerBouquet;
import epam.model.bouquet.bouquetImpl.birthday.ClassicTulipBouqete;

public class LvivShop extends ShopFactory{

    @Override
    protected Bouquet createBouquet(BouquetType bouquetType) {
        switch (bouquetType){
            case DUBLER: return new DublerBouquet();
            case CLASSIC_TULIP: return new ClassicTulipBouqete();
            case CLASSIC_ROSE: return new ClassicRoseBouquet();
            case BIRTHDAY: return new BirthdayBouquet();
        }
        return null;
    }
}
