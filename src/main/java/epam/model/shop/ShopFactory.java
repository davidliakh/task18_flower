package epam.model.shop;

import epam.model.bouquet.Bouquet;
import epam.model.bouquet.BouquetBasis;
import epam.model.bouquet.BouquetType;
import epam.model.bouquet.event.EventType;
import epam.model.bouquet.event.impl.BirthdayEvent;
import epam.model.bouquet.event.impl.FuneralEvent;
import epam.model.bouquet.event.impl.StValentinesEvent;
import epam.model.bouquet.event.impl.WeddingEvent;

import java.util.*;

public abstract class ShopFactory {

    public List<Bouquet> getEventBouquetList(EventType type){
        switch (type){
            case FUNERAL:
                return new FuneralEvent().getEventBouquets();
            case WEDDING:
                return new WeddingEvent().getEventBouquets();
            case BIRTHDAY:
                return new BirthdayEvent().getEventBouquets();
            case ST_VALENTINES:
                return new StValentinesEvent().getEventBouquets();
        }
        return null;
    }

    protected abstract Bouquet createBouquet(BouquetType bouquetType);

    public Bouquet getBouquet(BouquetType bouquetType){
        Bouquet bouquet = createBouquet(bouquetType);
        return bouquet;
    }

    public Bouquet getCustomBouquet(BouquetType bouquetType){
        Bouquet bouquet = createBouquet(bouquetType);
        return bouquet;
    }
}
