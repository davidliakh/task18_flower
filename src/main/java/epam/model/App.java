package epam.model;

import epam.model.bouquet.Bouquet;
import epam.model.bouquet.bouquetImpl.birthday.BirthdayBouquet;
import epam.model.observer.Card;
import epam.model.decor.BouquetDecorator;
import epam.model.decor.Decoration;
import epam.model.decor.decorImplementation.*;
import epam.model.view.ShopView;

import java.util.List;

public class App {

    public static void main(String[] args) {
       new ShopView().runMenu();
    }
}
