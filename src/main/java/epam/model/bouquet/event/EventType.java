package epam.model.bouquet.event;

public enum EventType {
    WEDDING("Wedding"), FUNERAL("Funeral"),
    BIRTHDAY("Birthday"), ST_VALENTINES("St Valentines");

    private String name;

    EventType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
