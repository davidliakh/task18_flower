package epam.model.bouquet.event.impl;

import epam.model.bouquet.Bouquet;
import epam.model.bouquet.bouquetImpl.st_valentines.ClassicStValentinesBouquet;
import epam.model.bouquet.bouquetImpl.st_valentines.RomanticStValentinesBouquet;
import epam.model.bouquet.bouquetImpl.st_valentines.TulipStValentinesBouquet;
import epam.model.bouquet.event.EventHandler;


import java.util.ArrayList;
import java.util.List;

public class StValentinesEvent implements EventHandler {

    @Override
    public List<Bouquet> getEventBouquets() {
        List<Bouquet> bouquets = new ArrayList<>();
        bouquets.add(new ClassicStValentinesBouquet());
        bouquets.add(new RomanticStValentinesBouquet());
        bouquets.add(new TulipStValentinesBouquet());
        return bouquets;
    }
}
