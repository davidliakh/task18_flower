package epam.model.bouquet.event.impl;

import epam.model.bouquet.Bouquet;
import epam.model.bouquet.bouquetImpl.birthday.BirthdayBouquet;
import epam.model.bouquet.bouquetImpl.birthday.ClassicTulipBouqete;
import epam.model.bouquet.bouquetImpl.birthday.ExtraBirthdayBouquet;
import epam.model.bouquet.bouquetImpl.birthday.ExtraLargeBirthdayBouquet;
import epam.model.bouquet.event.EventHandler;

import java.util.ArrayList;
import java.util.List;

public class BirthdayEvent implements EventHandler {

    @Override
    public List<Bouquet> getEventBouquets() {
        List<Bouquet> bouquets = new ArrayList<>();
        bouquets.add(new BirthdayBouquet());
        bouquets.add(new ClassicTulipBouqete());
        bouquets.add(new ExtraBirthdayBouquet());
        bouquets.add(new ExtraLargeBirthdayBouquet());
        return bouquets;
    }
}
