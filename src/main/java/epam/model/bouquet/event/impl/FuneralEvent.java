package epam.model.bouquet.event.impl;

import epam.model.bouquet.Bouquet;
import epam.model.bouquet.event.EventHandler;
import epam.model.bouquet.bouquetImpl.funeral.ClassicFuneralBouquet;
import epam.model.bouquet.bouquetImpl.funeral.ExtraFuneralBouquet;
import epam.model.bouquet.bouquetImpl.funeral.ExtraLargeFuneralBouquet;

import java.util.ArrayList;
import java.util.List;

public class FuneralEvent implements EventHandler {

    @Override
    public List<Bouquet> getEventBouquets() {
        List<Bouquet> bouquets = new ArrayList<>();
        bouquets.add(new ClassicFuneralBouquet());
        bouquets.add(new ExtraLargeFuneralBouquet());
        bouquets.add(new ExtraFuneralBouquet());
        return bouquets;
    }
}
