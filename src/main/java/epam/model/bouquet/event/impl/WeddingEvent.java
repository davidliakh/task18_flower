package epam.model.bouquet.event.impl;

import epam.model.bouquet.Bouquet;
import epam.model.bouquet.event.EventHandler;
import epam.model.bouquet.bouquetImpl.wedding.ArtificialBouquet;
import epam.model.bouquet.bouquetImpl.wedding.ClassicRoseBouquet;
import epam.model.bouquet.bouquetImpl.wedding.DublerBouquet;

import java.util.ArrayList;
import java.util.List;

public class WeddingEvent implements EventHandler {

    @Override
    public List<Bouquet> getEventBouquets() {
        List<Bouquet> bouquets = new ArrayList<>();
        bouquets.add(new ArtificialBouquet());
        bouquets.add(new ClassicRoseBouquet());
        bouquets.add(new DublerBouquet());
        return bouquets;
    }
}
