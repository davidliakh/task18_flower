package epam.model.bouquet.event;

import epam.model.bouquet.Bouquet;

import java.util.List;

public interface EventHandler {

    List<Bouquet> getEventBouquets();
}
