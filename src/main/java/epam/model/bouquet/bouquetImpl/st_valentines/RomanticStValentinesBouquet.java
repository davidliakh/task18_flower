package epam.model.bouquet.bouquetImpl.st_valentines;

import epam.model.bouquet.BouquetBasis;
import epam.model.bouquet.BouquetType;
import epam.model.bouquet.Flower;

public class RomanticStValentinesBouquet extends BouquetBasis {
    public RomanticStValentinesBouquet() {
        super(BouquetType.ROMANTIC);
        addKindOfFlower(Flower.RED_ROSE,50);
        addKindOfFlower(Flower.WHITE_ROSE,51);
        calculatePrice();
    }
}
