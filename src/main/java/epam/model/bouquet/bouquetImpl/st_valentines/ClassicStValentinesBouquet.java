package epam.model.bouquet.bouquetImpl.st_valentines;

import epam.model.bouquet.BouquetBasis;
import epam.model.bouquet.BouquetType;
import epam.model.bouquet.Flower;

public class ClassicStValentinesBouquet extends BouquetBasis {
    public ClassicStValentinesBouquet() {
        super(BouquetType.CLASSIC_TULIP);
        addKindOfFlower(Flower.TULIP,101);
        calculatePrice();
    }
}
