package epam.model.bouquet.bouquetImpl.st_valentines;

import epam.model.bouquet.BouquetBasis;
import epam.model.bouquet.BouquetType;
import epam.model.bouquet.Flower;

public class TulipStValentinesBouquet extends BouquetBasis {
    public TulipStValentinesBouquet() {
        super(BouquetType.CLASSIC_TULIP);
        addKindOfFlower(Flower.TULIP,55);
        calculatePrice();
    }
}
