package epam.model.bouquet.bouquetImpl.birthday;

import epam.model.bouquet.BouquetBasis;
import epam.model.bouquet.BouquetType;
import epam.model.bouquet.Flower;

public class ClassicTulipBouqete extends BouquetBasis {
    public ClassicTulipBouqete() {
        super(BouquetType.CLASSIC_TULIP);
        addKindOfFlower(Flower.TULIP,7);
        calculatePrice();
    }
}
