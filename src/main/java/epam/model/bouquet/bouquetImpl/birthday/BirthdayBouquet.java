package epam.model.bouquet.bouquetImpl.birthday;

import epam.model.bouquet.BouquetBasis;
import epam.model.bouquet.BouquetType;
import epam.model.bouquet.Flower;

public class BirthdayBouquet extends BouquetBasis {
    public BirthdayBouquet() {
        super(BouquetType.BIRTHDAY);
        addKindOfFlower(Flower.WHITE_ROSE,15);
        calculatePrice();
    }
}
