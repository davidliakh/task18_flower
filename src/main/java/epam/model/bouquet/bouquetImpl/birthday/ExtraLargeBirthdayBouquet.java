package epam.model.bouquet.bouquetImpl.birthday;

import epam.model.bouquet.BouquetBasis;
import epam.model.bouquet.BouquetType;
import epam.model.bouquet.Flower;

public class ExtraLargeBirthdayBouquet extends BouquetBasis {
    public ExtraLargeBirthdayBouquet() {
        super(BouquetType.EXTRA_LARGE);
        addKindOfFlower(Flower.MAGNOLIA,5);
        calculatePrice();
    }
}
