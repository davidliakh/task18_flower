package epam.model.bouquet.bouquetImpl.birthday;

import epam.model.bouquet.BouquetBasis;
import epam.model.bouquet.BouquetType;
import epam.model.bouquet.Flower;

public class ExtraBirthdayBouquet extends BouquetBasis {
    public ExtraBirthdayBouquet() {
        super(BouquetType.EXTRA);
        addKindOfFlower(Flower.RED_ROSE,7);
        addKindOfFlower(Flower.WHITE_ROSE,15);
        calculatePrice();
    }
}
