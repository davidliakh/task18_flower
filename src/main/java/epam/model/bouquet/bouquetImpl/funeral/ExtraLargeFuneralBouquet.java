package epam.model.bouquet.bouquetImpl.funeral;

import epam.model.bouquet.BouquetBasis;
import epam.model.bouquet.BouquetType;
import epam.model.bouquet.Flower;

public class ExtraLargeFuneralBouquet extends BouquetBasis {
    public ExtraLargeFuneralBouquet() {
        super(BouquetType.EXTRA_LARGE);
        addKindOfFlower(Flower.MAGNOLIA,15);
        addKindOfFlower(Flower.PEONY,15);
        calculatePrice();
    }
}
