package epam.model.bouquet.bouquetImpl.funeral;

import epam.model.bouquet.BouquetBasis;
import epam.model.bouquet.BouquetType;
import epam.model.bouquet.Flower;

public class ClassicFuneralBouquet extends BouquetBasis {
    public ClassicFuneralBouquet() {
        super(BouquetType.CLASSIC);
        addKindOfFlower(Flower.RED_ROSE,10);
        calculatePrice();
    }
}
