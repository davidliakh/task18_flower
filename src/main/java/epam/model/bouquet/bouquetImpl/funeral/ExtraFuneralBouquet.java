package epam.model.bouquet.bouquetImpl.funeral;

import epam.model.bouquet.BouquetBasis;
import epam.model.bouquet.BouquetType;
import epam.model.bouquet.Flower;

public class ExtraFuneralBouquet extends BouquetBasis {
    public ExtraFuneralBouquet() {
        super(BouquetType.EXTRA_LARGE);
        addKindOfFlower(Flower.NARCISSUS,26);
        calculatePrice();
    }
}
