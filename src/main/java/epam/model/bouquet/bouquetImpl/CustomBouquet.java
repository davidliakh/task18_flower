package epam.model.bouquet.bouquetImpl;

import epam.model.bouquet.BouquetBasis;
import epam.model.bouquet.BouquetType;
import epam.model.bouquet.Flower;

public class CustomBouquet extends BouquetBasis {
    public CustomBouquet() {
        super(BouquetType.CUSTOM);
    }
}
