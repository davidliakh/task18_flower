package epam.model.bouquet.bouquetImpl.wedding;

import epam.model.bouquet.BouquetBasis;
import epam.model.bouquet.BouquetType;
import epam.model.bouquet.Flower;

public class ArtificialBouquet extends BouquetBasis {
    public ArtificialBouquet() {
        super(BouquetType.ARTIFICIAL);
        addKindOfFlower(Flower.ARTIFICIAL_FLOWER,13);
        calculatePrice();
    }
}
