package epam.model.bouquet.bouquetImpl.wedding;

import epam.model.bouquet.BouquetBasis;
import epam.model.bouquet.BouquetType;
import epam.model.bouquet.Flower;

public class DublerBouquet extends BouquetBasis {
    public DublerBouquet() {
        super(BouquetType.DUBLER);
        addKindOfFlower(Flower.LILIA,13);
        calculatePrice();
    }
}
