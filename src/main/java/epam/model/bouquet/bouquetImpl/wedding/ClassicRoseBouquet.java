package epam.model.bouquet.bouquetImpl.wedding;

import epam.model.bouquet.BouquetBasis;
import epam.model.bouquet.BouquetType;
import epam.model.bouquet.Flower;

public class ClassicRoseBouquet extends BouquetBasis {

    public ClassicRoseBouquet() {
        super(BouquetType.CLASSIC_ROSE);
        addKindOfFlower(Flower.RED_ROSE, 24);
        calculatePrice();
    }
}
