package epam.model.bouquet;

public enum Flower {
    NARCISSUS("Narcissus", 12), WHITE_ROSE("White rose", 24), TULIP("Tulip", 20.2),
    MAGNOLIA("Magnolia", 10), LILIA("Lilia", 14.5), RED_ROSE("Red rose", 26),
    PEONY("Peony", 12), ARTIFICIAL_FLOWER("Artificial Flower", 3);
    private String name;
    private double price;

    Flower(String name, double price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }
}