package epam.model.bouquet;

import java.util.List;
import java.util.Map;

public interface Bouquet {

    double getPrice();
    List<String> getDecorations();
    Map<Flower, Integer> getFlowers();
    BouquetType getBouquetType();
}
