package epam.model.bouquet;

public enum BouquetType {
    CLASSIC_ROSE("Classic rose"), ROMANTIC("Romantic"),
    KISS("Kiss"), WHITE_ROSES101("101 white roses"),
    RED_ROSES101("101 red roses"), EXTRA_LARGE("Extra large"),
    SWEET_LOVE("Sweet love"), CUSTOM("Custom"),
    DUBLER("Dubler"), ARTIFICIAL("Artificial"), EXTRA("Extra"),
    CLASSIC_TULIP("Classic tulip"), CLASSIC("Classic"), BIRTHDAY("Birthday");

    private String name;

    BouquetType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
