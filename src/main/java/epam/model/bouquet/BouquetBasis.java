package epam.model.bouquet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class BouquetBasis implements Bouquet{
    private BouquetType bouquetType;
    private Map<Flower,Integer> flowers;
    private double price;
    private List<String> decorations;

    public BouquetBasis(BouquetType bouquetType) {
        this.bouquetType = bouquetType;
        flowers = new HashMap<>();
        decorations = new ArrayList<>();
    }

    protected void addKindOfFlower(Flower flower, int numberOfFlowers){
        flowers.put(flower, numberOfFlowers);
    }

    public void calculatePrice(){
        for (Flower flower : flowers.keySet()) {
            price += flower.getPrice()* flowers.get(flower);
        }
    }

    @Override
    public double getPrice() {
        return price;
    }

    @Override
    public List<String> getDecorations() {
        return decorations;
    }

    public Map<Flower, Integer> getFlowers() {
        return flowers;
    }

    public BouquetType getBouquetType() {
        return bouquetType;
    }

    @Override
    public String toString() {
        return "Bouquet{" +
                "Type=" + bouquetType +
                ", flowers=" + flowers +
                ", price=" + price +
                ", decorations=" + decorations +
                '}';
    }
}
