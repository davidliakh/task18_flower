package epam.model.view;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class InputUtils {
    private static Logger log = LogManager.getLogger(InputUtils.class);

    public static int getUserInput(List<String> ordinalParameter) {
        Scanner scanner = new Scanner(System.in);
        String keyMenu;
        while (true){
            keyMenu = scanner.nextLine().toUpperCase();
            if (ordinalParameter.contains(keyMenu)){
                return Integer.parseInt(keyMenu);
            }else if (keyMenu.equals(Integer.toString(ordinalParameter.size()+1))){
                return Integer.parseInt(keyMenu);
            }else{
                log.trace("Wrong input, please choose a variant from list");
                log.trace(">>> ");
            }
        }
    }

    public static List<String> getEnumOrdinals(Enum[] enums){
        List<String> ordinals = new ArrayList<>();
        for (int i = 0; i < enums.length; i++) {
            ordinals.add(String.valueOf(enums[i].ordinal()+1));
        }
        return ordinals;
    }

    public static boolean getYesOrNot(){
        Scanner scanner = new Scanner(System.in);
        String keyMenu = "";
            while (!keyMenu.equalsIgnoreCase("Y")
                    && !keyMenu.equalsIgnoreCase("N")) {
                log.info("Enter (Y/N) >>> ");
                keyMenu = scanner.nextLine().toUpperCase();
            }
            return keyMenu.equalsIgnoreCase("Y");
    }

    public static boolean getDeliveryType(){
        Scanner scanner = new Scanner(System.in);
        String keyMenu = "";
            while (!keyMenu.equalsIgnoreCase("Y")
                    && !keyMenu.equalsIgnoreCase("N")) {
                log.info("Enter (Y/N) >>> ");
                keyMenu = scanner.nextLine().toUpperCase();
            }
            return keyMenu.equalsIgnoreCase("Y");
    }

}
