package epam.model.view;

import epam.model.bouquet.Bouquet;
import epam.model.bouquet.Flower;
import epam.model.bouquet.bouquetImpl.CustomBouquet;
import epam.model.bouquet.event.EventType;
import epam.model.decor.BouquetDecorator;
import epam.model.decor.Decoration;
import epam.model.facade.*;
import epam.model.observer.Card;
import epam.model.observer.Customer;
import epam.model.observer.Observable;
import epam.model.observer.Observer;
import epam.model.shop.City;
import epam.model.shop.DniproShop;
import epam.model.shop.KyivShop;
import epam.model.shop.LvivShop;
import epam.model.shop.ShopFactory;
import epam.model.util.PropertiesRead;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class ShopView implements Observable {
    private static Logger log = LogManager.getLogger(ShopView.class);
    private PropertiesRead read = new PropertiesRead("shop_menu.properties");
    private Scanner scanner;
    private ShopFactory factory;
    private Order order;
    private List<OrderFacade> orders;
    private Map<String, String> menu;
    private Map<String, Runnable> methodRun;
    private Observer customer;
    private BouquetDecorator bouquetDecorator;
    private List<Observer> customers;

    public ShopView() {
        customers = new ArrayList<>();
        bouquetDecorator = new BouquetDecorator();
        factory = new KyivShop();
        order = new Order();
        orders = new ArrayList<>();
        scanner = new Scanner(System.in);
        initMenu();
        initMethodRun();
    }

    public void runMenu() {
        log.info("---------- Welcome ----------");
        initClient();
        String flag = "";
        do {
            menu.values().forEach(log::info);
            try {
                flag = scanner.next();
                methodRun.get(flag).run();
            } catch (NullPointerException  | IllegalArgumentException e) {
                log.error("Input correct option");
            }
        } while (!flag.equals("6"));
    }

    private void initClient(){
        log.info("What is your name?");
        String userName = scanner.nextLine();
        log.info("What type of card do you have?");
        for (int i = 0; i < Card.values().length; i++) {
            log.info((i + 1) + " " + Card.values()[i]);
        }
        int userCard = InputUtils.getUserInput(
                InputUtils.getEnumOrdinals(Card.values()));
        try{
            customer = new Customer(userName, Card.values()[userCard-1]);
        } catch (ArrayIndexOutOfBoundsException e){
            customer = new Customer(userName, Card.BASIC);
        }
        customers.add(customer);
        changeCity();
    }

    private void initMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", read.getValue("city"));
        menu.put("2", read.getValue("event"));
        menu.put("3", read.getValue("custom"));
        menu.put("4", read.getValue("order"));
        menu.put("5", read.getValue("accept"));
        menu.put("6", read.getValue("exit"));
    }

    private void initMethodRun(){
        methodRun = new LinkedHashMap<>();
        methodRun.put("1", this::changeCity);
        methodRun.put("2", this::getBouquetFromEvent);
        methodRun.put("3", this::addCustomBouquet);
        methodRun.put("4", () -> order.getBouquets().forEach(log::info));
        methodRun.put("5", this::acceptOrder);
        methodRun.put("6", () -> log.info("Exit"));
    }

    private void changeCity(){
        log.info("Choose city:");
        for (int i = 0; i < City.values().length; i++) {
            log.info((i + 1) + " " + City.values()[i]);
        }
        log.info(">>>");
        int cityInt = InputUtils.getUserInput(
                InputUtils.getEnumOrdinals(City.values()));
        City city = City.values()[cityInt-1];
        switch (city) {
            case KYIV:
                factory = new KyivShop();
                break;
            case LVIV:
                factory = new LvivShop();
                break;
            case DNIPRO:
                factory = new DniproShop();
                break;
            default:
                factory = new KyivShop();
                log.warn("Default factory -> Kyiv");
        }
        notifyObserver(city);
    }

    private void getBouquetFromEvent(){
        int ret = EventType.values().length + 1;
        log.info("Choose event: ");
        for (int i = 0; i < EventType.values().length; i++) {
            log.info((i + 1) + " " + EventType.values()[i]);
        }
        log.info(ret + " Back");
        log.info(">>>");
        int event = InputUtils.getUserInput(
                InputUtils.getEnumOrdinals(EventType.values()));
        if (event != ret){
            EventType eventType = EventType.values()[event-1];
            addBouquetToOrder(factory.getEventBouquetList(eventType));
        }
    }

    private void addBouquetToOrder(List<Bouquet> bouquets) {
        for (int i = 0;i < bouquets.size();i++){
            log.info((i + 1 )+ " - "+ bouquets.get(i));
        }
        log.info("\nEnter number of bouquet : ");
        int index = scanner.nextInt();
        Bouquet bouquet = bouquets.get(index - 1);
        order.addBouquetToOrder(bouquet, customer.getCard());
        log.info(bouquet + "\n" + "Successfully added");
        notifyObserver(order);
    }

    private void addCustomBouquet(){
        Bouquet bouquet = new CustomBouquet();
        bouquet = addFlowers(bouquet);
        bouquet = decorateBouquet(bouquet);
        order.addBouquetToOrder(bouquet, customer.getCard());
        log.info("Custom bouquet successfully added");
        bouquetDecorator = new BouquetDecorator();
        notifyObserver(order);
    }

    private Bouquet addFlowers(Bouquet bouquet){
        bouquetDecorator.setBouquet(bouquet);
        int flower;
        int amount;
        int ret = Flower.values().length + 1;
        do {
            log.info("Choose flower: ");
            for (int i = 0; i < Flower.values().length; i++) {
                log.info((i + 1) + " " + Flower.values()[i]);
            }
            log.info(ret + " Back");
            log.info(">>>");
            flower = InputUtils.getUserInput(
                    InputUtils.getEnumOrdinals(Flower.values()));
            if (flower != ret){
                log.info("Input amount >>>");
                amount = scanner.nextInt();
                bouquetDecorator.add(new BouquetDecorator(Flower.values()[flower-1], amount));
                log.info("Flowers are added");
            }
        }while (flower != ret);
        log.info("Successful creation");
        return bouquetDecorator;
    }

    private void acceptOrder(){
        Box box = null;
        Delivery delivery = null;
        log.info("Box your order?");
        if (InputUtils.getYesOrNot()){
            box = new Box();
        }
        log.info("Delivery?");
        if (InputUtils.getYesOrNot()){
            int ret = DeliveryType.values().length + 1;
            log.info("Choose delivery: ");
            for (int i = 0; i < DeliveryType.values().length; i++) {
                log.info((i + 1) + " " + DeliveryType.values()[i]);
            }
            log.info(ret + " Without delivery");
            log.info(">>>");
            int deliveryInt = InputUtils.getUserInput(
                    InputUtils.getEnumOrdinals(DeliveryType.values()));
            if (deliveryInt != ret){
                delivery = new Delivery(DeliveryType.values()[deliveryInt-1]);
            }
        }
        OrderFacade orderFacade = new FinalOrder(order, box, delivery);
        orderFacade.createOrder();
        orders.add(orderFacade);
        order = new Order();
    }

    private Bouquet decorateBouquet(Bouquet bouquet){
        int ret = Decoration.values().length + 1;
        int decoration;
        do {
            log.info("Choose decoration:");
            for (int i = 0; i < Decoration.values().length; i++) {
                log.info((i + 1) + " " + Decoration.values()[i]);
            }
            log.info(ret + " Back");
            log.info(">>>");
            decoration = InputUtils.getUserInput(
                    InputUtils.getEnumOrdinals(Decoration.values()));
            if (decoration != ret){
                bouquetDecorator.add(new BouquetDecorator(Decoration.values()[decoration-1]));
                log.info("Decoration added");
            }
        }while (decoration != ret);
        log.info("Successful decorated");
        return bouquetDecorator;
    }

    @Override
    public void addObserver(Observer observer) {
        this.customers.add(observer);
    }

    @Override
    public void removeObserver(Observer observer) {
        this.customers.remove(observer);
    }

    @Override
    public void notifyObserver(Order order) {
        order.getPrice();
        customer.handleEvent(order);
    }

    @Override
    public void notifyObserver(City city) {
        customer.handleEvent(city);
    }
}
