package epam.model.decor;

import java.util.ResourceBundle;

public enum Decoration {
    LINE("Line"), CARTON("Carton"), PERFUME("Perfume"),
    SPARKLES("Sparkles");

    private String name;
    private double price;

    Decoration(String name) {
        ResourceBundle resourceBundle = ResourceBundle.getBundle("prices");
        this.name = name;
        this.price = Double.parseDouble(resourceBundle.getString(name));
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }
}
