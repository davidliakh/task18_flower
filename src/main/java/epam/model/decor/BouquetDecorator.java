package epam.model.decor;

import epam.model.bouquet.Bouquet;
import epam.model.bouquet.BouquetType;
import epam.model.bouquet.Flower;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public class BouquetDecorator implements Bouquet {
    protected Optional<Bouquet> bouquet;
    protected double additionalPrice;
    private String additionalDecoration;
    private Integer flowersNumber;
    private Flower additionalFlower;

    public BouquetDecorator() {
    }

    public BouquetDecorator(Decoration decoration) {
        setAdditionalDecoration(decoration.getName());
        setAdditionalPrice(decoration.getPrice());
    }

    public BouquetDecorator(Flower flower, Integer amount) {
        setAdditionalFlower(flower, amount);
        setAdditionalPrice(flower.getPrice()*amount);
    }

    public void setBouquet(Bouquet bouquet) {
        this.bouquet = Optional.ofNullable(bouquet);
        if (additionalDecoration != null) {
            this.bouquet.orElseThrow(IllegalArgumentException::new).getDecorations().add(additionalDecoration);
        }
        if (additionalFlower != null) {
            Map<Flower, Integer> flowers = this.bouquet
                    .orElseThrow(IllegalArgumentException::new)
                    .getFlowers();
            if (flowers.containsKey(additionalFlower)){
                flowers.put(additionalFlower,
                        flowers.get(additionalFlower)+flowersNumber);
            }else{
                flowers.put(additionalFlower, flowersNumber);
            }
        }
    }

    public void add(BouquetDecorator bouquetDecorator){
        bouquetDecorator.setBouquet(bouquet.orElseThrow(NullPointerException::new));
        setBouquet(bouquetDecorator);
    }

    private void setAdditionalPrice(double price) {
        this.additionalPrice = price;
    }

    private void setAdditionalDecoration(String decoration) {
        this.additionalDecoration = decoration;
    }

    private void setAdditionalFlower(Flower flower, Integer amount) {
        this.additionalFlower = flower;
        this.flowersNumber = amount;
    }

    public Bouquet getBouquet(){
        return bouquet.orElse(null);
    }

    @Override
    public double getPrice() {
        return bouquet.orElseThrow(IllegalArgumentException::new).getPrice()
                + additionalPrice;
    }

    @Override
    public List<String> getDecorations() {
        return bouquet.orElseThrow(IllegalArgumentException::new).getDecorations();
    }

    @Override
    public Map<Flower, Integer> getFlowers() {
        return bouquet.orElseThrow(IllegalArgumentException::new).getFlowers();
    }

    @Override
    public BouquetType getBouquetType() {
        return BouquetType.CUSTOM;
    }

    @Override
    public String toString() {
        return "Bouquet{" +
                "bouquetType=" + getBouquetType() +
                ", flowers=" + getFlowers() +
                ", price=" + getPrice() +
                ", decorations='" + getDecorations() + '\'' +
                '}';
    }
}
