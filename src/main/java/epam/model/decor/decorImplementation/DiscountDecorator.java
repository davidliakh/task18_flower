package epam.model.decor.decorImplementation;

import epam.model.bouquet.Bouquet;
import epam.model.observer.Card;
import epam.model.decor.BouquetDecorator;

import java.util.Optional;

public class DiscountDecorator extends BouquetDecorator {
    private final Card card;

    public DiscountDecorator(Card card) {
        this.card = card;
    }

    @Override
    public void setBouquet(Bouquet bouquet) {
        this.bouquet = Optional.ofNullable(bouquet);
        setAdditionalPrice(card);
    }

    private void setAdditionalPrice(Card card) {
        this.additionalPrice = getPrice() * card.getDiscount() * (-1);
    }

    @Override
    public String toString() {
       return super.toString();
    }
}
